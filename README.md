# Gmail: delete old emails automatically after 90 days

Automatically deletes old emails regardless of their label

## Get started

- Create a new Google Apps Script at [https://script.google.com](https://script.google.com)
- Paste the code
- create a trigger to run daily


## Changelog

### 2019-09-25

- Created